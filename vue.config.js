const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  // publicPath: '/conference/2024/', // 部署应用包时的基本 URL( vue-router hash 模式使用./) ('/app/': vue-router history模式使用)
  publicPath: "./",
});
